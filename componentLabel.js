const labelTemplate = document.createElement('template')
labelTemplate.innerHTML =
`
<style></style>

<label>0</label>
`
class componentLabel extends HTMLElement{
    constructor() {
        super()
        this.root = this.attachShadow({mode: 'open'})
        this.root.appendChild(labelTemplate.content.cloneNode(true))
    }

    static get observedAttributes(){
        return ['result']
    }

    attributeChangedCallback(name, _oldVal, newVal){
        if (name === 'result'){
            this.root.querySelector('label').innerHTML = newVal
        }
    }
}

window.customElements.define('component-label', componentLabel)