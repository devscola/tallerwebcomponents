const buttonTemplate = document.createElement('template')
buttonTemplate.innerHTML = 
`
<style>
</style>

<button></button>
`

class ComponentButton extends HTMLElement{
  constructor(){
    super()
    this.root = this.attachShadow({mode: 'open'})
    this.root.appendChild(buttonTemplate.content.cloneNode(true))

    this.button = this.root.querySelector('button')
    this.button.addEventListener('click', (event)=>{
      this.sendEvent()
    })
  }

  static get observedAttributes(){
    return ['name', 'eventName']
  }

  get eventName(){
    return this.getAttribute('eventName')
  }

  attributeChangedCallback(name, _oldVal, newVal){
    if (name === 'name'){
      this.root.querySelector('button').innerHTML = newVal
    }
  }

  sendEvent(){
    this.dispatchEvent (
      new CustomEvent(this.eventName,{
        bubbles: true
      })
    )
  }
}

window.customElements.define('component-button', ComponentButton)